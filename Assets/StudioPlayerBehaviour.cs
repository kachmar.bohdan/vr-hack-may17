﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class StudioPlayerBehaviour : MonoBehaviour
{
    public RectTransform ScoresContainer;
    public RectTransform Cursor;

    private Sequence CursorSequence;

    private float LastCursorY;

    private bool firstSlide = true;

    // Use this for initialization
    void Start()
    {
        Play();
    }

    private void Play()
    {
        GenerateCursorSequence();
        GetComponent<AudioSource>().Play();
    }

    private void GenerateCursorSequence()
    {
        LastCursorY = Cursor.localPosition.y;

        CursorSequence = DOTween.Sequence();
        CursorSequence.OnComplete(OnPartitionSequenceEvent);
        DOTween.defaultEaseType = Ease.Linear;

        Partition CurrentPartitionData = new Partition(XMLParser.GetPartitionWithXMLData(Partition.TIMING_XML, 19));

        float currentSequenceTime = CurrentPartitionData.Measures[0].TimeStart;

        for (int i = 0; i < CurrentPartitionData.Measures.Count; i++)
        {
            for (int j = 0; j < CurrentPartitionData.Measures[i].Notes.Count; j++)
            {
                // Add notes
                Vector3 targetPos = new Vector3(CurrentPartitionData.Measures[i].Notes[j].PosX, CurrentPartitionData.Measures[i].PosY);
                float duration = CurrentPartitionData.Measures[i].Notes[j].Timing - currentSequenceTime;
                CursorSequence.Append(Cursor.DOLocalMove(targetPos, duration)
                                    .SetEase(Ease.Linear));

                currentSequenceTime = CurrentPartitionData.Measures[i].Notes[j].Timing;
            }
            // Add measure end
            // We do not use measure Start because it is the same as measure end

            Vector3 targetPos1 = new Vector3(CurrentPartitionData.Measures[i].PosEndX, CurrentPartitionData.Measures[i].PosY);
            float duration1 = CurrentPartitionData.Measures[i].TimeEnd - currentSequenceTime;
            CursorSequence.Append(Cursor.DOLocalMove(targetPos1, duration1)
                            .SetEase(Ease.Linear).OnComplete(OnMeasureSequenceEvent));

            currentSequenceTime = CurrentPartitionData.Measures[i].TimeEnd;
        }

        CursorSequence.Play();
    }

    public void OnMeasureSequenceEvent()
    {
        StartCoroutine(TrackNextLine());
    }

    private IEnumerator TrackNextLine()
    {
        yield return new WaitForSeconds(0.1f);
        if (LastCursorY != Cursor.localPosition.y)
        {
            if (!firstSlide)
            {
                Debug.Log("FOUND NEW LINE !!!");

                ScoresContainer.DOLocalMoveY(ScoresContainer.localPosition.y + 180.0f, 0.1f);

                yield return new WaitForSeconds(0.5f);
                LastCursorY = Cursor.localPosition.y;
            }
            else
            {
                firstSlide = false;
                LastCursorY = Cursor.localPosition.y;
            }
        }
    }

    public void OnPartitionSequenceEvent()
    {
        GetComponent<AudioSource>().Stop();
        Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
