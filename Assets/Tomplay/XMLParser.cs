﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public static class XMLParser
{
    // Use this for initialization
    public static Partition GetPartitionWithXMLData(string xmlString, int numberOfLines)
    {
		//Debug.Log("GetPartitionWithXMLData: " + xmlString);
		
		XmlDocument xml = new XmlDocument ();

		try
		{
			// In case the file is UTF-8 without BOM
			StringReader stringReader = new StringReader(xmlString);
			xml.LoadXml(stringReader.ReadToEnd());
		}
		catch
		{
			// In case the file is UTF-8 with BOM
			xml = new XmlDocument ();
			StringReader stringReader = new StringReader(xmlString);
			stringReader.Read(); // skip BOM 
			xml.LoadXml(stringReader.ReadToEnd());
		}

        Partition partition = new Partition(0, int.Parse(xml.DocumentElement.GetAttribute("tempo")));
        
        for (int i = 0; i < xml.DocumentElement.ChildNodes.Count; i++)
        {
            //Debug.Log("begin = " + xml.DocumentElement.ChildNodes[i].Attributes["begin"].Value);
            //Debug.Log("end = " + xml.DocumentElement.ChildNodes[i].Attributes["end"].Value);
            //Debug.Log("beginX = " + xml.DocumentElement.ChildNodes[i].Attributes["beginX"].Value);
            //Debug.Log("width = " + xml.DocumentElement.ChildNodes[i].Attributes["width"].Value);

            Measure measure = new Measure(i,
                float.Parse(xml.DocumentElement.ChildNodes[i].Attributes["begin"].Value),
                float.Parse(xml.DocumentElement.ChildNodes[i].Attributes["end"].Value),
                float.Parse(xml.DocumentElement.ChildNodes[i].Attributes["beginX"].Value) * 0.75f,
                float.Parse(xml.DocumentElement.ChildNodes[i].Attributes["beginX"].Value) * 0.75f + 
                float.Parse(xml.DocumentElement.ChildNodes[i].Attributes["width"].Value) * 0.75f,
                -1.0f);

            for (int j = 0; j < xml.DocumentElement.ChildNodes[i].ChildNodes.Count; j++ )
            {
                float currentNoteTiming = float.Parse(xml.DocumentElement.ChildNodes[i].ChildNodes[j].Attributes["timing"].Value);

                if (currentNoteTiming < 0.01f)
                {
                    currentNoteTiming = 0.01f;
                }

                Note note = new Note(j, float.Parse(xml.DocumentElement.ChildNodes[i].ChildNodes[j].Attributes["pos"].Value) * 0.75f, currentNoteTiming);
                measure.Notes.Add(note);
                //Debug.Log("pos = " + xml.DocumentElement.ChildNodes[i].ChildNodes[j].Attributes["pos"].Value);
                //Debug.Log("timing = " + xml.DocumentElement.ChildNodes[i].ChildNodes[j].Attributes["timing"].Value);
            }

            partition.Measures.Add(measure);
        }


		// Get measures posY data
		float currentMeasureYPosition = 180.0f * (numberOfLines - 1);
		float currentMeasureXPosition = -1.0f;

        for (int i = 0; i<partition.Measures.Count; i++)
        {
            // Track new line
            if (partition.Measures[i].PosStartX<currentMeasureXPosition)
            {
                currentMeasureYPosition -= 180.0f;
            }

            partition.Measures[i].PosY = currentMeasureYPosition;
            currentMeasureXPosition = partition.Measures[i].PosStartX;
        }

        return partition;
    }

}
