﻿using UnityEngine;
using System.Collections;

public class Note
{
    /// <summary>
    /// Inside each new measure note Id starts from 0
    /// </summary>
    public int Id;
    public float PosX;
    public float Timing;

    public Note(int id, float posX, float timing)
    {
        Id = id;
        PosX = posX;
        Timing = timing;
    }

    public Note(Note n)
    {
        Id = n.Id;
        PosX = n.PosX;
        Timing = n.Timing;
    }
}
