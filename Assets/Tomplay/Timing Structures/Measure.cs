﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Measure
{
    /// <summary>
    /// Data from XML file
    /// </summary>

    /// <summary>
    /// Measure Id is global and doesn't depend on line
    /// </summary>
    public int Id;
    public float TimeStart;
    public float TimeEnd;
    public float PosStartX;
    public float PosEndX;

    /// <summary>
    /// Data from actual score line image position
    /// </summary>
    public float PosY;

    /// <summary>
    /// Notes inside measure
    /// </summary>
    public List<Note> Notes;

    public Measure(int id, float timeStart, float timeEnd, float posStartX, float posEndX, float posY)
    {
        Id = id;
        TimeStart = timeStart;
        TimeEnd = timeEnd;
        PosStartX = posStartX;
        PosEndX = posEndX;
        PosY = posY;

        Notes = new List<Note>();
    }

    public Measure(Measure m)
    {
        Id = m.Id;
        TimeStart = m.TimeStart;
        TimeEnd = m.TimeEnd;
        PosStartX = m.PosStartX;
        PosEndX = m.PosEndX;
        PosY = m.PosY;

        Notes = new List<Note>();
        for (int i = 0; i < m.Notes.Count; i++)
        {
            Notes.Add(new Note(m.Notes[i]));
        }
    }

    public void SetPosY(float posY)
    {
        PosY = posY;
    }
}
